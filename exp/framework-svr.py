import pandas as pd
import numpy as np
from sklearn.model_selection import ParameterGrid
from sklearn.svm import SVR
from sklearn.impute import SimpleImputer
from evaluation import sa
from sklearn import preprocessing
from sklearn.model_selection import RepeatedKFold


data = pd.read_csv("desharnais.csv", na_values="?")
data = pd.get_dummies(data, columns=["Language"])
i = list(data.index)
np.random.shuffle(i)
data = data.iloc[i,]


imp = SimpleImputer(missing_values=np.nan, strategy='mean')
data = pd.DataFrame(imp.fit_transform(data), columns = data.columns)

X = data.drop(["Project","Effort"], axis=1)
X = pd.DataFrame(preprocessing.scale(X), columns = X.columns)
Y = preprocessing.scale(data["Effort"])


params_svr = {
    "kernel":["rbf"],
    "C":[50, 100, 500],
    "epsilon":[0.1, 0.01, 0.001],
    "gamma":[0.1, 0.01, 0.001]
    }

j = 0
res = []
for j in range(0, 100, 1):
    
    
    grid = list(ParameterGrid(params_svr))
    np.random.shuffle(grid)
    
    for param in grid:
        svm = SVR(**param)
        sa_score = 0
        cv = RepeatedKFold(n_repeats = 1, n_splits=10)
        for train_idx, test_idx in cv.split(X):
            X_train, X_test = X.iloc[train_idx,], X.iloc[test_idx,]
            Y_train, Y_test = Y[train_idx], Y[test_idx]
            svm.fit(X_train, Y_train)
            sa_score += sa(svm, X_test, Y_test)
        sa_score /= cv.get_n_splits()
        res.append([param["C"], param["epsilon"], param["gamma"], j, sa_score])
    

        
res = pd.DataFrame(res, columns=["C", "epsilon", "gamma", "kFold", "SA"])
res.to_csv("../data/svr.csv")

        

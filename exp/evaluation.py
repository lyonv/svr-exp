import numpy as np
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import make_scorer



def marp0_fit(actual, nruns=1000):
    res = 0
    std = 0
    n = actual.size
    actual = np.array(actual)
    samples = []
    # Depending on size of sample
    # Use original MARP0 by Shepperd and MacDonell
    # Or use unbiased version by Langdon et al.
    if n <= 2000:
        # Unbiased
        for i in range(0, n):
            for j in range(0, i):
                res += abs( actual[i] - actual[j] )
                samples.append(actual[j])
        res *= 2.0 / ( n ** 2 )
        
    else:
        # Original
        samples = []
        for i in range(0, n):
            p = [0 if x == i else 1/(actual.size - 1) for x in range(actual.size)]
            pred = np.random.choice(actual, nruns, replace=True, p=p)
            samples.extend(pred)
            res +=  np.abs( np.average(pred) - actual[i] )
        res /= n
    
    std = np.std(samples)
    return res, std

def sa_function(y_true, y_pred):
    return 1 - mean_absolute_error(y_true, y_pred) / marp0_fit(y_true)[0]

sa = make_scorer(sa_function, greater_is_better=True)
